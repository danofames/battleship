(function() {

    var row_count = 10,
        cell_count = 10,
        games = {},
        game_index = 0,
        cells_clicked = [],
        gameboard_el = document.getElementById('gameboard'),
        startbtn = document.getElementById('gamestartbtn'),
        ships = [5, 4, 3, 3, 2];

    startbtn.addEventListener('click', onStartGameClick);
    gameboard_el.addEventListener('click', onGameboardClick);

    // while developing
    // onStartGameClick();

    function onStartGameClick(event) {
        startGame(row_count, cell_count);
        gameboard_el.classList.add('board--started');
        (event && event.target || startbtn).parentNode.classList.add('gamestart--started');
    }

    function startGame(row_count, cell_count) {
        games[game_index] = {
            cells_clicked: [],
            ships: {},
            board: createGameboardData(row_count, cell_count)
        };

        var row_el = createGameboardRow(cell_count);
        for (var row = 0; row < row_count; row++) {
            // deep should be true so cells are cloned as well
            var new_row_el = row_el.cloneNode(true);
            new_row_el.dataset.row_index = row;
            gameboard_el.appendChild(new_row_el);
        }

        for (var ship = 0; ship < ships.length; ship++) {
            // all vertical for now
            var ship_placement = placeShip(ship, ships[ship], [0, ship]);
        }
    }

    function onGameboardClick(event) {
        var clicked_cell = event.target,
            row_index = clicked_cell.parentNode.dataset.row_index,
            cell_index = clicked_cell.dataset.cell_index;

        games[game_index].cells_clicked.push([row_index, cell_index]);

        games[game_index].board[row_index][cell_index].exploded = true;
        clicked_cell.classList.add('board__cell--exploded');

        if (clicked_cell.dataset.ship_id) {
            games[game_index].board[row_index][cell_index].hit = true;
            clicked_cell.classList.add('board__cell--hit');
        }
    }

    function createGameboardData(row_count, cell_count) {
        // don't use fill with object, will fill with same object
        var gameboard_row = [];
        for (var i = 0; i < cell_count; i++) {
            gameboard_row[i] = {};
        }
        return Array(row_count).fill(gameboard_row.slice(0));
    }

    function createGameboardRow(cell_count) {
        var row_el = document.createElement('tr');
        row_el.classList.add('board__row');

        for (var cell = 0; cell < cell_count; cell++) {
            var cell_el = document.createElement('td');
            cell_el.classList.add('board__cell');
            cell_el.dataset.cell_index = cell;
            row_el.appendChild(cell_el);
        }

        return row_el;
    }

    function placeShip(ship_id, ship_length, start_coordinates) {
        var start_row = start_coordinates[0],
            start_col = start_coordinates[1];

        if (!hasShip(start_row, start_col, ship_length)) {
            for (var i = 0; i < ship_length; i++) {
                games[game_index].board[start_row + i][start_col].has_ship = true;
                gameboard_el.children[start_row + i].children[start_col].classList.add('board__cell--hasship');
                gameboard_el.children[start_row + i].children[start_col].dataset.ship_id = ship_id;
            }
        }
    }

    function hasShip(start_row, start_col, ship_length) {
        for (var i = 0; i < ship_length; i++) {
            if (games[game_index].board[start_row + i][start_col].has_ship === true) {
                return true;
            }
        }
        return false;
    }

})();
