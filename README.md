# Handshake Battleship

To view battleship game, run in directory of working copy:
```
python -mSimpleHTTPServer
```

Open browser to http://localhost:8000/


# Notes

- Writing code is often how I get a better sense of the challenge, and work out decisions on design and approach. This work would be about where I would take a step back and evaluate before proceeding on the more difficult aspects of the solution.
- Not intimately familiar with getting test runners set up, so did not start with tests due to time limits.
- Started with layout of board and simple placement of ships to get warmed up.

# Next steps

- Algorithmic placement of ships with checks on no overlapping ships and no overhang off board
- Determine whether ship has been sunk
- Have just started with Angular 2 and getting familiar with the reactive/redux concepts. Initial approach was more along with old habits, but keep wanting this to be more driven by data, then layout changes.
- Additional 'nice to haves' features